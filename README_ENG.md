![Full_Logo](Full_Logo.PNG)

# Crossroads: Cursed Aftifacts and Ritual Services

## At the **Crossroads** You have three ways, or in our case -- three tiers:

1. **Rabbit paw** (Things of Luck and Low level Magickry)
    -- Nice and _safe_

We accept almost any currency, but preferably Dooblons, Złoty or Euro; All the prices are in Euro, for other kinds of payment -- contact our customer support. 

2. **Monkey paw** (Cursed Items and Ghost Communication)
    -- Requires a certain level of _work experience_ (**Partake in our proficiency test**)

Because most of those products are of questionable nature -- the prices vary from regular currency to pure blood, organs, cattle, etcetera. About the methods of payment -- contact our customer support.

3. **Human paw** (Extremely Dangerous Blood Rituals and Artifacts)
    -- _Requires_ **the proficiency test** and signing the document of _**Denial of Responsibility**_
    
As payment we only accept _human souls_. Administration is not responsible for your attempts to pay for those purchases with sacrifices.

## Special offers:

- In each of our categories periodically appear _Products from our **Partners**_. Few of the most well known our partners are -- **"Needful things &Co."** and **"INC-visition"**.

- We collaborate with many otherwordly influencers, so while you shop on our playform -- you can enter a promocode from your **Patron** and get a _discount_ and open _access to categories 2 and 3_.

- Our _**regulars**_ get : _Free delivery_ and _Professional help_ with your rituals, and also _discount_ for such our services as _Cult PR_ and _Ressurection from Horcrux(tm)_. (For a full pricelist of our _Ritual Services_ -- contact our customer support.)

